import { Config } from '@jest/types';
import defaultConfig from './jest.unit-tests.config';

const ciConfig: Config.InitialOptions = {
  ...defaultConfig,
  ci: true,
  notify: false,
  collectCoverage: true,
  reporters: ['default', 'jest-junit'],
};

export default ciConfig;
