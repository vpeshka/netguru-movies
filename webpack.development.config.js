const path = require('path');
const nodeExternalsPlugin = require('webpack-node-externals');
const packageInfo = require(path.join(__dirname + '/package.json'));
const tsPathsPlugin = require('tsconfig-paths-webpack-plugin');

module.exports = {
  name: packageInfo.name,
  target: 'node',
  mode: 'development',
  devtool: 'source-map',
  node: {
    __dirname: false,
    __filename: false,
  },
  entry: {
    app: './src/cluster.ts',
  },
  output: {
    filename: `service.js`,
    path: path.join(__dirname, 'dist'),
  },
  experiments: {
    topLevelAwait: true,
  },
  externals: [
    nodeExternalsPlugin(),
  ],
  module: {
    exprContextCritical: false,
    rules: [{
      test: /\.ts$/,
      exclude: /node_modules/,
      use: [{
        loader: 'ts-loader',
      }],
    }],
  },
  resolve: {
    extensions: ['.ts'],
    plugins: [new tsPathsPlugin()],
  },
};
