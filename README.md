# Movies Service

### Service provide functionality for creating movies based on title

### 1. Installation and Running.

#### 1.1 Docker environment.
For running application on your local machine we recommended use Docker.
For start application with Docker execute following command:
```bash
docker-compose -f docker-compose.development.yml up --build --force-recreate
```
This command will create following containers:
```
- movies-service (serve node application)
- movies-db (database)
- movies-rl (redis storage for rate liniting)
```

More details about Docker you can read [here](https://docs.docker.com/).

#### 1.2 Local environment.

You also can serve application on your host. For running on your host be sure that 
you have installed NodeJS v.14+ and declare following envs
(we are strongly recommend you to use **.env** file from _/deployment/local/docker.env_ file for that):
```
PORT=3000
NODE_ENV=development
CPU_SCALING_ENABLED=false
LOG_LEVEL=debug
JWT_EXPIRES_IN=3600
JWT_SIGN_ALGORITHM=HS256
JWT_SECRET=some-secret
API_KEY=some-key
MOVIES_DB_CONN_STRING=conn-string
RL_REDIS_HOST=host
RL_REDIS_PORT=6379
```

As our service has written in TypeScript before run it you have to compile source code to JavaScript.
For that, you can run following command (under hood is webpack job):
```bash
npm run dist:development
```

And finally you can start application by next command:
```bash
node ./dist/service.js
```

## API Documentation
> API documentation written in [RAML](https://raml.org/) format and located in folder `documentation` in project root.

To get a human-readable representation of API description, do following steps:
- Install [raml2html](https://www.npmjs.com/package/raml2html) NPM package on your system (globally):
  ```shell
  npm i -g raml2html
  ```
- Run following command to generate HTML file:
  ```shell
  cd ./documentation
  raml2html ./movies-service.raml > index.html
  ```
- Open [generated html file](./documentation/index.html) in your favorite browser.
