import { join } from 'path';
import type { Config } from '@jest/types';
import { pathsToModuleNameMapper } from 'ts-jest';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const { compilerOptions } = require(join(__dirname, 'tsconfig.json'));

const paths = pathsToModuleNameMapper(compilerOptions.paths || {}, {
  prefix: '<rootDir>/',
}) || {};

// Mock DI container
paths['^@app/ioc/container$'] = '<rootDir>/tests/unit/mocks/container.mock';

const config: Config.InitialOptions = {
  // An array of glob patterns indicating a set of files for which coverage information should be collected
  collectCoverageFrom: [
    'src/**',
    '!src/container.ts',
    '!src/server.ts',
    '!src/cluster.ts',
    '!src/app/app.ts',
    '!src/**/routers/**.ts',
    '!src/**/enums/**.ts',
    '!src/**/interfaces/**.ts',
    '!src/**/**.types.ts',
    '!src/**/**.config.ts',
    '!src/**/index.ts',
    '!node_modules/**',
  ],
  // The directory where Jest should output its coverage files
  coverageDirectory: 'coverage',
  // A list of reporter names that Jest uses when writing coverage reports
  coverageReporters: [
    'text',
    'html',
    'cobertura',
  ],
  // Make calling deprecated APIs throw helpful error messages
  errorOnDeprecated: false,
  // An array of directory names to be searched recursively up from the requiring module's location
  moduleDirectories: [
    'node_modules',
  ],
  moduleNameMapper: paths,
  // An array of file extensions your modules use
  moduleFileExtensions: ['ts', 'js'],
  // Activates notifications for test results
  notify: true,
  // The test environment that will be used for testing
  testEnvironment: 'node',
  // The glob patterns Jest uses to detect test files
  testMatch: [
    '<rootDir>/**/**.test.ts',
  ],
  // An array of regexp pattern strings that are matched against all test paths, matched tests are skipped
  testPathIgnorePatterns: [
    'node_modules/',
  ],
  // A map from regular expressions to paths to transformers
  transform: {
    '^.+\\.(ts)$': ['ts-jest', {
      tsconfig: 'tsconfig.json',
      diagnostics: false,
    }],
  },
  setupFiles: ['./jest.setup.ts'],
};

export default config;
