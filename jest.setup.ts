import { config as dotEnvConfig } from 'dotenv';

dotEnvConfig({ path: './deployment/development/development.env' });
