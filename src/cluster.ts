import 'reflect-metadata';
import * as Promise from 'bluebird';
global.Promise = Promise as unknown as PromiseConstructor;
import { cpus } from 'os';
import cluster from 'cluster';
import { container } from './container';
import { parseEnvAsBoolean } from '@vp_solutions/express-skeleton-common-module';
import { ILoggerService, LOGGER_SERVICE } from '@vp_solutions/express-skeleton-logger-module';

const logger = container.get<ILoggerService>(LOGGER_SERVICE);

if (cluster.isPrimary && parseEnvAsBoolean(process.env.CPU_SCALING_ENABLED, true)) {
  logger.info(`[Cluster] Master is running. Pid: '${ process.pid }'.`);

  for (const {} of cpus()) {
    cluster.fork();
  }

  cluster.on('online', (worker) => {
    logger.info(`[Cluster] Worker is listening. Pid: '${ worker.process.pid }'.`);
  });

  cluster.on('exit', (worker, code, signal) => {
    logger.info(
      '[Cluster] Worker is died.\n' +
      `Pid: '${ worker.process.pid }'.\n`,
      `Code: '${ code }'.\n`,
      `Signal: '${ signal }'.\n`,
    );
  });
} else {
  import('./server');
  logger.info(`[Cluster] Worker is started. Pid: '${ process.pid }'.`);
}
