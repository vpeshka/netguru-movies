import { Container, interfaces } from 'inversify';
import { getAuthModule } from '@app/features/auth';
import { getDbClientModule } from '@app/shared/db';
import { getRateLimitModule } from '@app/shared/rl';
import { getMoviesModule } from '@app/features/movies';
import { getGuardModule } from '@vp_solutions/express-skeleton-guard-module';
import { getLoggerModule } from '@vp_solutions/express-skeleton-logger-module';
import { getHealthModule } from '@vp_solutions/express-skeleton-health-module';

const container: interfaces.Container = new Container();

container.load(
  getLoggerModule(),
  getGuardModule(),
  getHealthModule(),
  getDbClientModule(),
  getAuthModule(),
  getMoviesModule(),
  getRateLimitModule(),
);

export { container };
