import * as http from 'http';
import { app } from './app/app';
import { container } from './container';
import { RedisClientType } from 'redis';
import { RATE_LIMIT_STORE } from '@app/shared/rl';
import { InversifyExpressServer } from 'inversify-express-utils';
import { IMoviesDbClient, MOVIES_DB_CLIENT } from '@app/shared/db';
import { ServerTools, parseEnvAsNumber } from '@vp_solutions/express-skeleton-common-module';
import { ILoggerService, LOGGER_SERVICE } from '@vp_solutions/express-skeleton-logger-module';

export const APP_PORT = parseEnvAsNumber(process.env.PORT || 3000);

const inversifyServer = new InversifyExpressServer(container, null, null, app, null, false);
const server = http.createServer(inversifyServer.build());

const logger = container.get<ILoggerService>(LOGGER_SERVICE);
server.on('listening', ServerTools.getOnListeningListener(server, APP_PORT, logger));
server.on('error', ServerTools.getOnErrorListener(server, APP_PORT, logger));
server.listen(APP_PORT);

await container.get<IMoviesDbClient>(MOVIES_DB_CLIENT).establishConnection();
await container.get<RedisClientType>(RATE_LIMIT_STORE).connect();

process.on('uncaughtException', ServerTools.getUncaughtExceptionListener(logger));
process.on('unhandledRejection', ServerTools.getUnhandledRejectionListener(logger));
