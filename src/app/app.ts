import * as express from 'express';
import { container } from '../container';
import * as compression from 'compression';
import { AUTH_ROUTER } from '@app/features/auth';
import { MOVIES_ROUTER } from '@app/features/movies';
import { getExpressErrorHandler } from '@vp_solutions/errors';
import { IBaseRouter } from '@vp_solutions/express-skeleton-common-module';
import { GUARD_ROUTER } from '@vp_solutions/express-skeleton-guard-module';
import { HEALTH_ROUTER } from '@vp_solutions/express-skeleton-health-module';
import { ILoggerService, LOGGER_SERVICE } from '@vp_solutions/express-skeleton-logger-module';

const app = express();
app.disable('x-powered-by');
app.use(compression());

app.use('/auth', container.get<IBaseRouter>(AUTH_ROUTER).router);
app.use('/movies', container.get<IBaseRouter>(MOVIES_ROUTER).router);
app.use('/health', container.get<IBaseRouter>(HEALTH_ROUTER).router);
app.use('*', container.get<IBaseRouter>(GUARD_ROUTER).router);
app.use(getExpressErrorHandler(container.get<ILoggerService>(LOGGER_SERVICE)));

export { app };
