import { interfaces } from 'inversify';
import { IMoviesDbClient } from './db.client.interface';

export const MOVIES_DB_CLIENT: interfaces.ServiceIdentifier<IMoviesDbClient> = Symbol.for('MOVIES_DB_CLIENT');
