import 'reflect-metadata';
import { MongoClient } from 'mongodb';
import { container } from '@app/ioc/container';
import { MOVIES_DB_CLIENT } from './db.client.token';
import { getDbClientModule } from './db.client.module';
import { IMoviesDbClient } from './db.client.interface';
import { getLoggerModule } from '@vp_solutions/express-skeleton-logger-module';
import { mustNotToBeCalledAssertion } from '@vp_solutions/express-skeleton-common-module';

jest.mock('mongodb', () => {
  return {
    MongoClient: jest.fn().mockReturnValue({
      db: () => {
        return { isDbMock: true };
      },
      connect: jest.fn().mockResolvedValue(undefined),
    }),
  };
});

describe('DB Client test cases.', () => {
  beforeAll(() => {
    container.unbindAll();

    container.load(getLoggerModule(), getDbClientModule());
  });

  afterAll(() => {
    container.unbindAll();
  });

  it('Method IMoviesDbClient::getDb() must resolve promise if mongo client resolve promise successfully.', async () => {
    await container.get<IMoviesDbClient>(MOVIES_DB_CLIENT).establishConnection();
    expect(true).toBeTruthy();

  });

  it('Method IMoviesDbClient::getDb() must reject promise if mongo client reject error.', async () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore MongoClient is mocked instance
    (new MongoClient().connect as jest.Mock).mockRejectedValueOnce(undefined);

    try {
      await container.get<IMoviesDbClient>(MOVIES_DB_CLIENT).establishConnection();
      mustNotToBeCalledAssertion();
    } catch (err) {
      expect(true).toBeTruthy();
    }
  });

  it('Method IMoviesDbClient::getDb() must return Db instance.', () => {
    expect(container.get<IMoviesDbClient>(MOVIES_DB_CLIENT).getDb()).toEqual({ isDbMock: true });
  });
});
