import 'reflect-metadata';
import { DbClient } from './db.client';
import { ContainerModule } from 'inversify';
import { container } from '@app/ioc/container';
import { MOVIES_DB_CLIENT } from './db.client.token';
import { getDbClientModule } from './db.client.module';
import { getLoggerModule } from '@vp_solutions/express-skeleton-logger-module';

jest.mock('mongodb');

describe('DB Client module test cases.', () => {
  beforeAll(() => {
    container.unbindAll();

    container.load(getLoggerModule());
  });

  afterAll(() => {
    container.unbindAll();
  });

  it('Func getDbClientModule() must return instance of ContainerModule.', () => {
    expect(getDbClientModule()).toBeInstanceOf(ContainerModule);
  });

  it('Container module returned by func getDbClientModule() must provide valid instances of types.', () => {
    container.load(getDbClientModule());

    expect(container.get(MOVIES_DB_CLIENT)).toBeInstanceOf(DbClient);
  });
});

