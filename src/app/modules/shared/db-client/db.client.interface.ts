import { Db } from 'mongodb';

export interface IMoviesDbClient {
  getDb(): Db;
  establishConnection(): Promise<void>;
}
