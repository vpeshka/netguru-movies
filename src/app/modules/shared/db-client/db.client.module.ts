import { DbClient } from './db.client';
import { MOVIES_DB_CLIENT } from './db.client.token';
import { ContainerModule, interfaces } from 'inversify';
import { IMoviesDbClient } from './db.client.interface';

export const getDbClientModule = (): interfaces.ContainerModule => {
  return new ContainerModule((bind: interfaces.Bind) => {
    bind<IMoviesDbClient>(MOVIES_DB_CLIENT).to(DbClient).inSingletonScope();
  });
};
