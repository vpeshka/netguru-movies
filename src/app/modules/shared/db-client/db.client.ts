import { Db, MongoClient } from 'mongodb';
import { IMoviesDbClient } from './db.client.interface';
import { inject, injectable, postConstruct } from 'inversify';
import { MOVIES_DB_CONN_STRING } from './db.client.module.config';
import { ILoggerService, LOGGER_SERVICE } from '@vp_solutions/express-skeleton-logger-module';

@injectable()
export class DbClient implements IMoviesDbClient {
  private mongoClient: MongoClient;

  @inject<ILoggerService>(LOGGER_SERVICE)
  private readonly logger: ILoggerService;

  public getDb(): Db {
    return this.mongoClient.db();
  }

  public async establishConnection(): Promise<void> {
    try {
      await this.mongoClient.connect();

      this.logger.debug(`[MoviesDbClient] Connection successfully created.`);
    } catch (err) {
      this.logger.error(`[MoviesDbClient] Error during create connection: ${ err }.`);

      throw err;
    }
  }

  @postConstruct()
  protected initializeClient(): void {
    this.mongoClient = new MongoClient(MOVIES_DB_CONN_STRING, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });
  }
}
