import 'reflect-metadata';
import * as moment from 'moment';
import { Response } from 'express';
import * as redis from 'redis-mock';
import { createClient } from 'redis';
import { RateLimit } from './rate-limit';
import { container } from '@app/ioc/container';
import { RATE_LIMIT } from './rate-limit.token';
import { IRateLimit } from './rate-limit.interface';
import { RateLimitError } from '@vp_solutions/errors';
import { RATE_LIMIT_STORE } from './rate-limit.store.token';
import { IAuthenticatedRequest, UserRole } from '@app/features/auth';

jest.mock('redis', () => redis);

describe('Rate limit test cases', () => {
  let limiter: IRateLimit;
  const nextMock = jest.fn();
  const resMock = {} as Response;
  const reqMock: IAuthenticatedRequest = {
    getJwtData: jest.fn(),
  } as unknown as IAuthenticatedRequest;
  const redisClientMock: any = {
    ...createClient(),
    store: {} as Record<string, string>,
    get: jest.fn(),
    set: async (key: string, val: string) => {
      redisClientMock.store = { ...redisClientMock.store, [key]: val };
    },
  };

  beforeAll(() => {
    container.snapshot();

    container.bind(RATE_LIMIT_STORE).toConstantValue(redisClientMock);
    container.bind(RATE_LIMIT).to(RateLimit).inSingletonScope();

    limiter = container.get(RATE_LIMIT);
  });

  beforeEach(() => {
    nextMock.mockClear();
    (reqMock.getJwtData as jest.Mock).mockClear();

    (redisClientMock.get as jest.Mock).mockReset();
    redisClientMock.get = jest.fn().mockImplementation(async (key: string) => redisClientMock.store[key]);
  });

  afterAll(() => {
    container.restore();
  });

  it('If stored rl have window of previous month, limit must be reset.', async () => {
    // Mock required values
    (reqMock.getJwtData as jest.Mock).mockReturnValue({ userId: 123, role: UserRole.Basic });
    (redisClientMock.get as jest.Mock).mockResolvedValue(JSON.stringify({
      firstEntryTimestamp: moment().subtract(1, 'months').unix(),
      requestsCount: 6,
    }));

    await limiter.limit(reqMock, resMock, nextMock);
    expect(nextMock).toBeCalledTimes(1);
    expect(nextMock).toBeCalledWith();
  });

  it('Limit for premium user must be never achieved.', async () => {
    (reqMock.getJwtData as jest.Mock).mockReturnValue({ userId: 321, role: UserRole.Premium });

    for (let i = 0; i < 10; i++) {
      await limiter.limit(reqMock, resMock, nextMock);

      expect(nextMock).toBeCalledTimes(i + 1);
      expect(nextMock).toBeCalledWith();
    }
  });

  it('Limit for basic user must achieved after 5 requests.', async () => {
    (reqMock.getJwtData as jest.Mock).mockReturnValue({ userId: 123, role: UserRole.Basic });

    for (let i = 0; i < 10; i++) {
      await limiter.limit(reqMock, resMock, nextMock);
      expect(nextMock).toBeCalledTimes(i + 1);

      if (i > 4) {
        expect(nextMock).toBeCalledWith(new RateLimitError('You exceed limit for the operation for current month.'));
      } else {
        expect(nextMock).toBeCalledWith();
      }
    }
  });
});
