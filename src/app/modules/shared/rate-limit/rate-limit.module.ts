import { RateLimit } from './rate-limit';
import { RATE_LIMIT } from './rate-limit.token';
import { IRateLimit } from './rate-limit.interface';
import { createClient, RedisClientType } from 'redis';
import { interfaces, ContainerModule } from 'inversify';
import { RATE_LIMIT_STORE } from './rate-limit.store.token';
import { RL_REDIS_STORE_HOST, RL_REDIS_STORE_PORT } from './rate-limit.config';

export const getRateLimitModule = (): interfaces.ContainerModule => {
  return new ContainerModule((bind: interfaces.Bind) => {
    bind<RedisClientType>(RATE_LIMIT_STORE)
      .toConstantValue(createClient({ url: `redis://${ RL_REDIS_STORE_HOST }:${ RL_REDIS_STORE_PORT }` }));
    bind<IRateLimit>(RATE_LIMIT).to(RateLimit).inSingletonScope();
  });
};

