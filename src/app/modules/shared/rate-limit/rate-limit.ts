import * as moment from 'moment';
import { RedisClientType } from 'redis';
import { inject, injectable } from 'inversify';
import { NextFunction, Response } from 'express';
import { IRateLimit } from './rate-limit.interface';
import { RateLimitError } from '@vp_solutions/errors';
import { RATE_LIMIT_STORE } from './rate-limit.store.token';
import { IAuthenticatedRequest, IJWTData, UserRole } from '@app/features/auth';

@injectable()
export class RateLimit implements IRateLimit {
  @inject<RedisClientType>(RATE_LIMIT_STORE)
  private readonly store: RedisClientType;

  public async limit(req: IAuthenticatedRequest, _res: Response, next: NextFunction): Promise<void> {
    // Do not use rate limiting for premium users
    if (req.getJwtData<IJWTData>().role === UserRole.Premium) {
      return next();
    }

    const record = await this.store.get(req.getJwtData<IJWTData>().userId.toString(10));
    const currentRequestTime = moment();
    const endOfLimit = moment().clone().endOf('month');

    if (record == null) {
      await this.processFirstEntry(req.getJwtData<IJWTData>().userId, currentRequestTime.unix());

      return next();
    }

    const data = JSON.parse(record);

    if (moment.unix(data.firstEntryTimestamp).clone().endOf('month').isSame(endOfLimit)) {
      if (data.requestsCount >= 5) {
        return next(new RateLimitError('You exceed limit for the operation for current month.'));
      }

      await this.store.set(
        req.getJwtData<IJWTData>().userId.toString(10),
        JSON.stringify(Object.assign({}, data, { requestsCount: data.requestsCount + 1 })),
      );

      return next();
    }

    await this.processFirstEntry(req.getJwtData<IJWTData>().userId, currentRequestTime.unix());

    return next();
  }

  private async processFirstEntry(userId: number, firstEntryTimestamp: number): Promise<void> {
    await this.store.set(userId.toString(10), JSON.stringify({ firstEntryTimestamp, requestsCount: 1 }));
  }
}
