import { parseEnvAsNumber } from '@vp_solutions/express-skeleton-common-module';

export const RL_REDIS_STORE_HOST: string = process.env.RL_REDIS_HOST as string;
export const RL_REDIS_STORE_PORT: number = parseEnvAsNumber(process.env.RL_REDIS_PORT);

