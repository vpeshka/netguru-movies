import { interfaces } from 'inversify';
import { IRateLimit } from './rate-limit.interface';

export const RATE_LIMIT: interfaces.ServiceIdentifier<IRateLimit> = Symbol.for('RATE_LIMIT');
