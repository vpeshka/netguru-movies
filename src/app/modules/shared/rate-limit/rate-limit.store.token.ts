import { interfaces } from 'inversify';
import { RedisClientType } from 'redis';

export const RATE_LIMIT_STORE: interfaces.ServiceIdentifier<RedisClientType> = Symbol.for('RATE_LIMIT_STORE');
