import { NextFunction, Request, Response } from 'express';

export interface IRateLimit {
  limit(req: Request, _res: Response, next: NextFunction): Promise<void>;
}
