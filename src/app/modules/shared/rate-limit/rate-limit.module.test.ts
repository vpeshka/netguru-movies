import 'reflect-metadata';
import * as redis from 'redis-mock';
import { RateLimit } from './rate-limit';
import { ContainerModule } from 'inversify';
import { container } from '@app/ioc/container';
import { RATE_LIMIT } from './rate-limit.token';
import { getRateLimitModule } from './rate-limit.module';
import { RATE_LIMIT_STORE } from './rate-limit.store.token';

jest.mock('redis', () => redis);

describe('Rate limit module test cases.', () => {
  beforeAll(() => {
    container.unbindAll();
  });

  afterAll(() => {
    container.unbindAll();
  });

  it('Func getRateLimitModule() must return instance of ContainerModule.', () => {
    expect(getRateLimitModule()).toBeInstanceOf(ContainerModule);
  });

  it('Container module returned by func getRateLimitModule() must provide valid instances of types.', () => {
    container.load(getRateLimitModule());

    expect(container.get(RATE_LIMIT_STORE)).not.toBeUndefined();
    expect(container.get(RATE_LIMIT)).toBeInstanceOf(RateLimit);
  });
});
