import { interfaces } from 'inversify';
import { IBaseRouter } from '@vp_solutions/express-skeleton-common-module';

export const AUTH_ROUTER: interfaces.ServiceIdentifier<IBaseRouter> = Symbol.for('AUTH_ROUTER');
