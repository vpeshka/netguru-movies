import { json } from 'body-parser';
import { injectable, postConstruct, inject } from 'inversify';
import { IAuthController, AUTH_CONTROLLER } from '../controllers';
import { BaseRouter, IBaseRouter, runAsync } from '@vp_solutions/express-skeleton-common-module';

@injectable()
export class AuthRouter extends BaseRouter implements IBaseRouter {
  @inject<IAuthController>(AUTH_CONTROLLER)
  private readonly authController: IAuthController;

  @postConstruct()
  protected defineRoutes(): void {
    this.router.post(
      '',
      json({ strict: true }),
      runAsync(this.authController.authenticateUser.bind(this.authController)),
    );
  }
}
