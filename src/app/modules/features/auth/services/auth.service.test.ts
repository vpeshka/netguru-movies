import 'reflect-metadata';
import { users } from '../seed';
import * as jwt from 'jsonwebtoken';
import { AuthService } from './auth.service';
import { container } from '@app/ioc/container';
import { Unauthorized } from '@vp_solutions/errors';
import { IAuthService } from './auth.service.interface';
import { EXPIRES_IN, ISSUER } from '../auth.module.config';
import { AUTH_SERVICE, JWT_SECRET } from './auth.service.token';
import { mustNotToBeCalledAssertion } from '@vp_solutions/express-skeleton-common-module';

jest.mock('jsonwebtoken', () => {
  return {
    sign: jest.fn(),
  };
});

describe('Auth service test cases.', () => {
  let service: IAuthService;

  beforeAll(() => {
    container.unbindAll();

    container.bind(JWT_SECRET).toConstantValue('some-secret');
    container.bind(AUTH_SERVICE).to(AuthService).inSingletonScope();

    service = container.get<IAuthService>(AUTH_SERVICE);
  });

  beforeEach(() => {
    (jwt.sign as jest.Mock).mockClear();
  });

  afterAll(() => {
    container.unbindAll();
  });

  it('Method IAuthService::authenticate() must call jsonwebtoken sign function with valid params.', () => {
    service.authenticate(users[0].username, users[0].password);

    expect(jwt.sign).toBeCalledTimes(1);
    expect(jwt.sign).toHaveBeenCalledWith({
      name: users[0].name,
      role: users[0].role,
      userId: users[0].id,
    }, 'some-secret', {
      expiresIn: EXPIRES_IN,
      issuer: ISSUER,
      subject: users[0].id.toString(),
    });
  });

  it('Method IAuthService::authenticate() must return result of jsonwebtoken sign function call.', () => {
    const tokenMock = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9';
    (jwt.sign as jest.Mock).mockReturnValue(tokenMock);

    expect(service.authenticate(users[0].username, users[0].password)).toEqual(tokenMock);
  });

  it('Method IAuthService::authenticate() must throw error if provided password doesn\'t match to stored.', () => {
    try {
      service.authenticate(users[0].username, users[0].password.slice(0, users[0].password.length - 1));
      mustNotToBeCalledAssertion();
    } catch (err: any) {
      expect(err).toBeInstanceOf(Unauthorized);
      expect(err.message).toEqual('invalid username or password');
    }
  });

  it('Method IAuthService::authenticate() must throw error if used was not found.', () => {
    try {
      service.authenticate('test', 'test');
      mustNotToBeCalledAssertion();
    } catch (err: any) {
      expect(err).toBeInstanceOf(Unauthorized);
      expect(err.message).toEqual('invalid username or password');
    }
  });
});
