import { users } from '../seed';
import * as jwt from 'jsonwebtoken';
import { IUser } from '../interfaces';
import { inject, injectable } from 'inversify';
import { JWT_SECRET } from './auth.service.token';
import { Unauthorized } from '@vp_solutions/errors';
import { IAuthService } from './auth.service.interface';
import { EXPIRES_IN, ISSUER } from '../auth.module.config';

@injectable()
export class AuthService implements IAuthService {
  @inject<Buffer | string>(JWT_SECRET)
  private readonly jwtSecret: Buffer | string;

  public authenticate(username: string, password: string): string {
    const user = users.find((u: IUser) => u.username === username);

    if (!user || user.password !== password) {
      throw new Unauthorized('invalid username or password');
    }

    return jwt.sign(
      {
        userId: user.id,
        name: user.name,
        role: user.role,
      },
      this.jwtSecret,
      {
        issuer: ISSUER,
        subject: `${ user.id }`,
        expiresIn: EXPIRES_IN,
      },
    );
  }
}
