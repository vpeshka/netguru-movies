export interface IAuthService {
  authenticate(username: string, password: string): string;
}
