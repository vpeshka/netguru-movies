import { interfaces } from 'inversify';
import { IAuthService } from './auth.service.interface';

/**
 * @private
 * @deprecated Internal token - for Auth Service usage only
 */
export const JWT_SECRET: interfaces.ServiceIdentifier<Buffer | string> = Symbol.for('JWT_SECRET');
export const AUTH_SERVICE: interfaces.ServiceIdentifier<IAuthService> = Symbol.for('AUTH_SERVICE');
