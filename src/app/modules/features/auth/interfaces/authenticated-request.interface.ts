import { Request } from 'express';

export interface IAuthenticatedRequest extends Request {
  getJwtData: <T>() => T;
}
