import { UserRole } from '../enums';

export interface IJWTData {
  userId: number;
  name: string;
  role: UserRole;
  iat: number;
  exp: number;
  iss: string;
  sub: string;
}
