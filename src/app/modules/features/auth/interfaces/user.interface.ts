import { UserRole } from '../enums';

export interface IUser {
  id: number;
  role: UserRole;
  name: string;
  username: string;
  password: string;
}
