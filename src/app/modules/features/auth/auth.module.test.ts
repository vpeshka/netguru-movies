import 'reflect-metadata';
import { ContainerModule } from 'inversify';
import { getAuthModule } from './auth.module';
import { container } from '@app/ioc/container';
import * as Config from './auth.module.config';
import { InternalServerError } from '@vp_solutions/errors';
import { mustNotToBeCalledAssertion } from '@vp_solutions/express-skeleton-common-module';

import { AUTH_ROUTER } from './routers';
import { AuthRouter } from './routers/auth.router';

import { AUTH_CONTROLLER } from './controllers';
import { AuthController } from './controllers/auth.controller';

import { AUTH_SERVICE } from './services';
import { AuthService } from './services/auth.service';
import { JWT_SECRET } from './services/auth.service.token';


import { AUTH_MIDDLEWARE } from './middlewares';

jest.mock('./auth.module.config', () => {
  return {
    JWT_SIGN_ALGORITHM: {},
    JWT_SECRET: {},
  };
});

describe('Auth module test cases.', () => {
  beforeAll(() => {
    container.unbindAll();
  });

  afterAll(() => {
    container.unbindAll();
  });

  it('Func getDbClientModule() must return instance of ContainerModule.', () => {
    expect(getAuthModule()).toBeInstanceOf(ContainerModule);
  });

  it('Container module returned by func getAuthModule() must provide valid instances of types.', () => {
    (Config as Record<string, unknown>).JWT_SECRET = process.env.JWT_SECRET;
    (Config as Record<string, unknown>).JWT_SIGN_ALGORITHM = 'HS256';
    container.load(getAuthModule());

    expect(container.get(AUTH_ROUTER)).toBeInstanceOf(AuthRouter);
    expect(container.get(AUTH_CONTROLLER)).toBeInstanceOf(AuthController);
    expect(container.get(AUTH_SERVICE)).toBeInstanceOf(AuthService);
    expect(container.get(JWT_SECRET)).toEqual(process.env.JWT_SECRET);
    expect(typeof container.get(AUTH_MIDDLEWARE)).toEqual('function');
  });

  describe('Func getAuthModule() must throw error if value wasn\'t set for ENV:', () => {
    beforeEach(() => {
      container.unbindAll();
    });

    it('JWT_SECRET', () => {
      (Config as Record<string, unknown>).JWT_SECRET = undefined;

      try {
        container.load(getAuthModule());
        mustNotToBeCalledAssertion();
      } catch (err: any) {
        expect(err).toBeInstanceOf(InternalServerError);
        expect(err.message).toEqual('Environment variable "JWT_SECRET" must be a string.');
      }
    });

    it('JWT_SIGN_ALGORITHM', () => {
      (Config as Record<string, unknown>).JWT_SECRET = process.env.JWT_SECRET;
      (Config as Record<string, unknown>).JWT_SIGN_ALGORITHM = undefined;

      try {
        container.load(getAuthModule());
        mustNotToBeCalledAssertion();
      } catch (err: any) {
        expect(err).toBeInstanceOf(InternalServerError);
        expect(err.message).toEqual('Environment variable "JWT_SIGN_ALGORITHM" must be a string.');
      }
    });
  });
});
