import 'reflect-metadata';
import { constants } from 'http2';
import { container } from '@app/ioc/container';
import { AuthController } from './auth.controller';
import { AUTH_SERVICE, IAuthService } from '../services';
import { AUTH_CONTROLLER } from './auth.controller.token';
import { NextFunction, Request, Response } from 'express';
import { IAuthController } from './auth.controller.interface';
import { BadRequest, InternalServerError } from '@vp_solutions/errors';
import { mustNotToBeCalledAssertion } from '@vp_solutions/express-skeleton-common-module';

describe('Auth controller test cases.', () => {
  const serviceMock: IAuthService = {
    authenticate: jest.fn(),
  };

  const reqMock = {
    body: {
      password: 'pass',
      username: 'user',
    },
  } as Request;

  let controller: IAuthController;

  beforeAll(() => {
    container.unbindAll();

    container.bind(AUTH_SERVICE).toConstantValue(serviceMock);
    container.bind(AUTH_CONTROLLER).to(AuthController).inSingletonScope();

    controller = container.get<IAuthController>(AUTH_CONTROLLER);
  });

  beforeEach(() => {
    (serviceMock.authenticate as jest.Mock).mockReset();
  });

  afterAll(() => {
    container.unbindAll();
  });

  it('Method IAuthService::authenticateUser() must throw error if body is empty or not defined.', () => {
    try {
      controller.authenticateUser({} as Request, {} as Response, jest.fn() as NextFunction);
      mustNotToBeCalledAssertion();
    } catch (err: any) {
      expect(err).toBeInstanceOf(BadRequest);
      expect(err.message).toEqual('invalid payload');
    }
  });

  it('Method IAuthService::authenticateUser() must throw error if username is not provided in payload.', () => {
    try {
      controller.authenticateUser({ body: { password: 'pass' } } as Request, {} as Response, jest.fn() as NextFunction);
      mustNotToBeCalledAssertion();
    } catch (err: any) {
      expect(err).toBeInstanceOf(BadRequest);
      expect(err.message).toEqual('invalid payload');
    }
  });

  it('Method IAuthService::authenticateUser() must throw error if password is not provided in payload.', () => {
    try {
      controller.authenticateUser({ body: { username: 'user' } } as Request, {} as Response, jest.fn() as NextFunction);
      mustNotToBeCalledAssertion();
    } catch (err: any) {
      expect(err).toBeInstanceOf(BadRequest);
      expect(err.message).toEqual('invalid payload');
    }
  });

  it('Method IAuthService::authenticateUser() must throw error if IAuthService::authenticate() throw error.', () => {
    try {
      (serviceMock.authenticate as jest.Mock).mockImplementation((..._params: unknown[]) => {
        throw new InternalServerError('test error');
      });

      controller.authenticateUser(
        reqMock,
        { status: jest.fn().mockReturnValue({ json: jest.fn() }) } as unknown as Response,
        jest.fn() as NextFunction,
      );

      mustNotToBeCalledAssertion();
    } catch (err: any) {
      expect(err).toBeInstanceOf(InternalServerError);
      expect(err.message).toEqual('test error');
    }
  });

  it('Method IAuthService::authenticateUser() must return Response with token value.', () => {
    (serviceMock.authenticate as jest.Mock).mockReturnValueOnce('token-mock');

    const jsonFuncMock = jest.fn();
    const resMock = {
      status: jest.fn().mockReturnValue({ json: jsonFuncMock }),
    } as unknown as Response;

    controller.authenticateUser(reqMock, resMock, jest.fn() as NextFunction);

    expect(resMock.status).toBeCalledTimes(1);
    expect(resMock.status).toBeCalledWith(constants.HTTP_STATUS_OK);

    expect(jsonFuncMock).toBeCalledTimes(1);
    expect(jsonFuncMock).toBeCalledWith({ token: 'token-mock' });
  });
});
