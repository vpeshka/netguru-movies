import { interfaces } from 'inversify';
import { IAuthController } from './auth.controller.interface';

export const AUTH_CONTROLLER: interfaces.ServiceIdentifier<IAuthController> = Symbol.for('AUTH_CONTROLLER');
