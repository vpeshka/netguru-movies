import { NextFunction, Request, Response } from 'express';

export interface IAuthController {
  authenticateUser(req: Request, res: Response, next: NextFunction): Response;
}
