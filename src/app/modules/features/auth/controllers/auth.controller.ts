import { inject, injectable } from 'inversify';
import { BadRequest } from '@vp_solutions/errors';
import { AUTH_SERVICE, IAuthService } from '../services';
import { NextFunction, Request, Response } from 'express';
import { IAuthController } from './auth.controller.interface';

@injectable()
export class AuthController implements IAuthController {
  @inject<IAuthService>(AUTH_SERVICE)
  private readonly authService: IAuthService;

  public authenticateUser(req: Request, res: Response, _next: NextFunction): Response {
    if (!req.body) {
      throw new BadRequest('invalid payload');
    }

    const { username, password } = req.body;

    if (!username || !password) {
      throw new BadRequest('invalid payload');
    }

    return res.status(200).json({ token: this.authService.authenticate(username, password) });
  }
}
