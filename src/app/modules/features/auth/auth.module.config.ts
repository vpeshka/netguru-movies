import { Algorithm } from 'jsonwebtoken';

export const EXPIRES_IN: number = parseInt(process.env.JWT_EXPIRES_IN as string, 10) || 30 * 60;
export const ISSUER = 'https://www.netguru.com/';
export const JWT_SECRET: string = process.env.JWT_SECRET as string;
export const JWT_SIGN_ALGORITHM: Algorithm = process.env.JWT_SIGN_ALGORITHM as Algorithm;
