import { RequestHandler } from 'express';
import { Algorithm, Secret } from 'jsonwebtoken';

export interface IAuthenticateMiddleware {
  (secretKey: Secret, signAlgorithm: Algorithm): RequestHandler;
}
