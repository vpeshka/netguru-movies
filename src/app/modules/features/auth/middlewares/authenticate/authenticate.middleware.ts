import { Unauthorized } from '@vp_solutions/errors';
import { IAuthenticatedRequest } from '../../interfaces';
import { Algorithm, Secret, VerifyOptions, verify } from 'jsonwebtoken';
import { NextFunction, Request, RequestHandler, Response } from 'express';
import { IAuthenticateMiddleware } from './authenticate.middleware.interface';

export const getAuthenticationMiddleware: IAuthenticateMiddleware = (
  secretKey: Secret,
  signAlgorithm: Algorithm,
): RequestHandler => {
  return async (req: Request, _res: Response, next: NextFunction): Promise<void> => {
    if (req.get('Authorization')) {
      try {
        const decodedToken: unknown = await verify(
          (req.get('Authorization') as string).slice('Bearer '.length),
          secretKey,
          { algorithms: [signAlgorithm] } as VerifyOptions,
        );

        Object.assign(req, { getJwtData: () => decodedToken } as IAuthenticatedRequest);

        return next();
      } catch (err: any) {
        throw new Unauthorized(err?.message || err);
      }
    }

    throw new Unauthorized('Authorization header is required.');
  };
};
