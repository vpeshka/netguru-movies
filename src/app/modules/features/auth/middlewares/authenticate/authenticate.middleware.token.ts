import { interfaces } from 'inversify';
import { RequestHandler } from 'express';

export const AUTH_MIDDLEWARE: interfaces.ServiceIdentifier<RequestHandler> = Symbol.for('AUTH_MIDDLEWARE');
