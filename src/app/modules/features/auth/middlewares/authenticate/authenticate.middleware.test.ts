import * as jwt from 'jsonwebtoken';
import { Request, Response } from 'express';
import { Unauthorized } from '@vp_solutions/errors';
import { IAuthenticatedRequest } from '../interfaces';
import { getAuthenticationMiddleware } from './authenticate.middleware';

jest.mock('jsonwebtoken', () => {
  return {
    verify: jest.fn().mockReturnValue({
      someKey: 'someString',
    }),
  };
});

describe('getAuthenticationMiddleware() middleware test cases.', () => {
  const reqMock = {
    get: jest.fn().mockReturnValue('Bearer token'),
  } as unknown as Request;

  beforeEach(() => {
    (reqMock.get as jest.Mock).mockClear();
    (jwt.verify as jest.Mock).mockClear();
  });

  it('getAuthenticationMiddleware() have to returns RequestHandler function.', () => {
    expect(getAuthenticationMiddleware('secret', 'HS256')).toBeInstanceOf(Function);
  });

  it('Auth middleware have to throw Unauthorized error if Authorization header is not provided.', async () => {
    (reqMock.get as jest.Mock).mockReturnValue(undefined);

    try {
      await getAuthenticationMiddleware('secret', 'HS256')(reqMock, {} as Response, jest.fn);
    } catch (err: any) {
      expect(err).toBeInstanceOf(Unauthorized);
      expect(err.message).toEqual('Authorization header is required.');
    }
  });

  it('Auth middleware have to throw Unauthorized error if given error during verifying JWT.', async () => {
    (jwt.verify as jest.Mock).mockRejectedValueOnce(new Error('jwt parse'));
    (reqMock.get as jest.Mock).mockReturnValue('bearer token');

    try {
      await getAuthenticationMiddleware('secret', 'HS256')(reqMock, {} as Response, jest.fn);
    } catch (err: any) {
      expect(err).toBeInstanceOf(Unauthorized);
      expect(err.message).toEqual('jwt parse');
    }
  });

  it(
    'Auth middleware have to add getJwtData function to request instance witch must return parsed JWT value.',
    async () => {
      const nextMiddleware = jest.fn();
      const jwtData = { someKey: 'someString' };
      (jwt.verify as jest.Mock).mockResolvedValueOnce(jwtData);

      expect((reqMock as IAuthenticatedRequest).getJwtData).toBeUndefined();
      await getAuthenticationMiddleware('secret', 'HS256')(reqMock, {} as Response, nextMiddleware);
      expect((reqMock as IAuthenticatedRequest).getJwtData()).toEqual(jwtData);
      expect(nextMiddleware).toBeCalledTimes(1);
      expect(nextMiddleware).toBeCalledWith();
    });
});
