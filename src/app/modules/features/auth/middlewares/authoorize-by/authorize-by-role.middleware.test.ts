import { Response } from 'express';
import { UserRole } from '../../enums';
import { IJWTData } from '../../interfaces';
import { Forbidden } from '@vp_solutions/errors';
import { authorizeByRole } from './authorize-by-role.middleware';
import { IAuthenticatedRequest } from '@vp_solutions/express-skeleton-common-module';

describe('authorizeByRole() middleware test cases.', () => {
  const jwt: IJWTData = {
    userId: 123,
    name: 'Basic Thomas',
    role: UserRole.Basic,
    iat: 1606221838,
    exp: 1606223638,
    iss: 'https://www.netguru.com/',
    sub: '123',
  };

  const reqMock: IAuthenticatedRequest = {
    getJwtData: jest.fn().mockReturnValue(jwt),
  } as unknown as IAuthenticatedRequest;

  beforeEach(() => {
    (reqMock.getJwtData as jest.Mock).mockClear();
  });

  it('authorizeByRole() have to returns RequestHandler function.', () => {
    expect(authorizeByRole()).toBeInstanceOf(Function);
  });

  it('If user haven\'t got required role Forbidden error must be passed to the next middleware.', () => {
    (reqMock.getJwtData as jest.Mock).mockResolvedValue(Object.assign({}, jwt, { role: 'unknown' }));
    const nextMock = jest.fn();

    authorizeByRole()(reqMock, {} as Response, nextMock);
    expect(nextMock).toBeCalledTimes(1);
    expect(nextMock).toBeCalledWith(new Forbidden('User not permitted for proceed this action.'));
  });

  it(
    'If given request witch not compatible with IAuthenticatedRequest, ' +
    'or jwtData is not valid, have to be returned Forbidden error.', () => {
      const nextMock = jest.fn();

      authorizeByRole()({} as IAuthenticatedRequest, {} as Response, nextMock);
      expect(nextMock).toBeCalledTimes(1);
      expect(nextMock).toBeCalledWith(new Forbidden('req.getJwtData is not a function'));
    });

  it('', () => {
    (reqMock.getJwtData as jest.Mock).mockReturnValue(jwt);
    const nextMock = jest.fn();

    authorizeByRole()(reqMock, {} as Response, nextMock);
    expect(nextMock).toBeCalledTimes(1);
    expect(nextMock).toBeCalledWith();
  });
});
