import { UserRole } from '../../enums';
import { IJWTData } from '../../interfaces';
import { Forbidden } from '@vp_solutions/errors';
import { NextFunction, RequestHandler, Response } from 'express';
import { IAuthenticatedRequest } from '@vp_solutions/express-skeleton-common-module';

export const authorizeByRole = (): RequestHandler => {
  return ((
    req: IAuthenticatedRequest,
    _res: Response,
    next: NextFunction,
  ): void => {
    try {
      if (Object.values(UserRole).includes(req.getJwtData<IJWTData>().role as UserRole)) {
        return next();
      }
    } catch (err: any) {
      return next(new Forbidden(err?.message || err));
    }

    return next(new Forbidden('User not permitted for proceed this action.'));
  }) as RequestHandler;
};
