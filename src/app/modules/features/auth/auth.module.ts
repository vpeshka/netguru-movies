import { RequestHandler } from 'express';
import { interfaces, ContainerModule } from 'inversify';
import { InternalServerError } from '@vp_solutions/errors';
import { IBaseRouter } from '@vp_solutions/express-skeleton-common-module';
import { JWT_SECRET as secret, JWT_SIGN_ALGORITHM } from './auth.module.config';

import { AUTH_ROUTER } from './routers';
import { AuthRouter } from './routers/auth.router';

import { AUTH_MIDDLEWARE } from './middlewares';
import { getAuthenticationMiddleware } from './middlewares/authenticate/authenticate.middleware';

import { AuthService } from './services/auth.service';
import { AUTH_SERVICE, IAuthService } from './services';
import { JWT_SECRET } from './services/auth.service.token';

import { AUTH_CONTROLLER, IAuthController } from './controllers';
import { AuthController } from './controllers/auth.controller';

export const getAuthModule = (): interfaces.ContainerModule => {
  return new ContainerModule((bind: interfaces.Bind) => {
    bind<IBaseRouter>(AUTH_ROUTER).to(AuthRouter).inSingletonScope();
    bind<IAuthService>(AUTH_SERVICE).to(AuthService).inSingletonScope();
    bind<IAuthController>(AUTH_CONTROLLER).to(AuthController).inSingletonScope();

    if (secret) {
      bind<Buffer | string>(JWT_SECRET).toConstantValue(secret);
    } else {
      throw new InternalServerError('Environment variable "JWT_SECRET" must be a string.');
    }

    if (JWT_SIGN_ALGORITHM) {
      bind<RequestHandler>(AUTH_MIDDLEWARE).toConstantValue(getAuthenticationMiddleware(secret, JWT_SIGN_ALGORITHM));
    } else {
      throw new InternalServerError('Environment variable "JWT_SIGN_ALGORITHM" must be a string.');
    }
  });
};
