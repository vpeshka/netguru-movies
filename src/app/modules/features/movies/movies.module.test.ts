import 'reflect-metadata';
import { ContainerModule } from 'inversify';
import { RATE_LIMIT } from '@app/shared/rl';
import { container } from '@app/ioc/container';
import { MOVIES_DB_CLIENT } from '@app/shared/db';
import { getMoviesModule } from './movies.module';
import { AUTH_MIDDLEWARE } from '@app/features/auth';
import { LOGGER_SERVICE } from '@vp_solutions/express-skeleton-logger-module';

import { MOVIES_ROUTER } from './routers';
import { MoviesRouter } from './routers/movies.router';

import { MOVIES_CONTROLLER } from './controllers';
import { MoviesController } from './controllers/movies.controller';

import { MOVIES_SERVICE } from './services';
import { MoviesService } from './services/movies.service';

import { MOVIES_REPOSITORY, OMDB_REPOSITORY } from './repositories';
import { OmdbRepository } from './repositories/omdb/omdb.repository';
import { MoviesRepository } from './repositories/movies/movies.repository';

describe('Movies module test cases.', () => {
  beforeAll(() => {
    container.unbindAll();
  });

  afterAll(() => {
    container.unbindAll();
  });

  it('Func getMoviesModule() must return instance of ContainerModule.', () => {
    expect(getMoviesModule()).toBeInstanceOf(ContainerModule);
  });

  it('Container module returned by func getMoviesModule() must provide valid instances of types.', () => {
    container.load(getMoviesModule());
    // Mock dependencies
    container.bind(MOVIES_DB_CLIENT).toConstantValue(jest.fn());
    container.bind(LOGGER_SERVICE).toConstantValue(jest.fn());
    container.bind(AUTH_MIDDLEWARE).toConstantValue(jest.fn());
    container.bind(RATE_LIMIT).toConstantValue({ limit: jest.fn() });

    expect(container.get(MOVIES_ROUTER)).toBeInstanceOf(MoviesRouter);
    expect(container.get(MOVIES_CONTROLLER)).toBeInstanceOf(MoviesController);
    expect(container.get(MOVIES_SERVICE)).toBeInstanceOf(MoviesService);
    expect(container.get(MOVIES_REPOSITORY)).toBeInstanceOf(MoviesRepository);
    expect(container.get(OMDB_REPOSITORY)).toBeInstanceOf(OmdbRepository);
  });
});
