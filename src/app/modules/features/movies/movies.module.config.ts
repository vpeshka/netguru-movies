export const OMDB_API_HOST = 'https://omdbapi.com';
export const OMDB_API_KEY = process.env.API_KEY as string;
