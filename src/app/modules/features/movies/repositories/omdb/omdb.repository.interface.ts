import { IMovie } from '../../interfaces';

export interface IOmdbRepository {
  fetchMovieDetails(title: string): Promise<IMovie>;
}
