import 'reflect-metadata';
import HttpClient from 'axios';
import { container } from '@app/ioc/container';
import { OmdbRepository } from './omdb.repository';
import { OMDB_REPOSITORY } from './omdb.repository.token';
import { IOmdbRepository } from './omdb.repository.interface';
import { BadGateway, InternalServerError } from '@vp_solutions/errors';
import { LOGGER_SERVICE } from '@vp_solutions/express-skeleton-logger-module';
import { mustNotToBeCalledAssertion } from '@vp_solutions/express-skeleton-common-module';

jest.mock('axios');

describe('OMDB repository test cases.', () => {
  let repository: IOmdbRepository;

  beforeAll(() => {
    container.unbindAll();

    container.bind(LOGGER_SERVICE).toConstantValue({ debug: jest.fn(), error: jest.fn() });
    container.bind(OMDB_REPOSITORY).to(OmdbRepository).inSingletonScope();

    repository = container.get<IOmdbRepository>(OMDB_REPOSITORY);
  });

  beforeEach(() => {
    (HttpClient.request as jest.Mock).mockClear();
  });

  afterAll(() => {
    container.unbindAll();
  });

  it('IOmdbRepository::fetchMovieDetails() must throw InternalServerError if meet unknown error.', async () => {
    (HttpClient.request as jest.Mock).mockRejectedValueOnce(new Error('internal'));

    try {
      await repository.fetchMovieDetails('movie');
      mustNotToBeCalledAssertion();
    } catch (err: any) {
      expect(err).toBeInstanceOf(InternalServerError);
      expect(err.message).toEqual('Internal Server Error.');
    }
  });

  it('IOmdbRepository::fetchMovieDetails() must throw BadGateway if meet Axios error.', async () => {
    (HttpClient.request as jest.Mock)
      .mockRejectedValueOnce(Object.assign({}, new Error('axios'), { isAxiosError: true }));

    try {
      await repository.fetchMovieDetails('movie');
      mustNotToBeCalledAssertion();
    } catch (err: any) {
      expect(err).toBeInstanceOf(BadGateway);
      expect(err.message).toEqual('Bad Gateway.');
    }
  });

  it('IOmdbRepository::fetchMovieDetails() must return IMovie in case of successful request.', async () => {
    (HttpClient.request as jest.Mock).mockResolvedValueOnce({
      data: {
        'Title': 'Blade Runner',
        'Released': '25 Jun 1982',
        'Genre': 'Action, Sci-Fi, Thriller',
        'Director': 'Ridley Scott',
      },
    });

    expect(await repository.fetchMovieDetails('movie')).toEqual({
      title: 'Blade Runner',
      released: '25 Jun 1982',
      genre: 'Action, Sci-Fi, Thriller',
      director: 'Ridley Scott',
    } as IMovie);
  });
});
