import { EOL } from 'os';
import HttpClient from 'axios';
import { escape } from 'querystring';
import { IMovie } from '../../interfaces';
import { inject, injectable } from 'inversify';
import { IOmdbRepository } from './omdb.repository.interface';
import { BadGateway, InternalServerError } from '@vp_solutions/errors';
import { OMDB_API_HOST, OMDB_API_KEY } from '../../movies.module.config';
import { ILoggerService, LOGGER_SERVICE } from '@vp_solutions/express-skeleton-logger-module';

@injectable()
export class OmdbRepository implements IOmdbRepository {
  @inject<ILoggerService>(LOGGER_SERVICE)
  private readonly loggerService: ILoggerService;

  public async fetchMovieDetails(title: string): Promise<IMovie> {
    try {
      this.loggerService.debug(`[OMDB Repository] Requesting OMDB Api...`);

      const resp = await HttpClient.request({
        method: 'GET',
        url: `${ OMDB_API_HOST }/?t=${ escape(title) }&apiKey=${ escape(OMDB_API_KEY) }`,
      });

      this.loggerService.debug(`[OMDB Repository] Request has been successfully resolved.`);

      return {
        title: resp.data.Title,
        released: resp.data.Released,
        genre: resp.data.Genre,
        director: resp.data.Director,
      };
    } catch (err: any) {
      this.loggerService.error(
        `[OMDB Repository] Error occurred during requesting OMDB Api:${ EOL }${ err.message }`,
      );

      if (err?.isAxiosError) {
        throw new BadGateway();
      }

      throw new InternalServerError();
    }
  }
}
