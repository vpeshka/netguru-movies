import { IMovie } from '../../interfaces';

export interface IMoviesRepository {
  fetchAll(): Promise<Array<IMovie & { _id: string }>>;
  create(movie: IMovie): Promise<void>;
}
