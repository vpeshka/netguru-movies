import { IMovie } from '../../interfaces';
import { inject, injectable } from 'inversify';
import { IMoviesRepository } from './movies.repository.interface';
import { IMoviesDbClient, MOVIES_DB_CLIENT } from '@app/shared/db';

@injectable()
export class MoviesRepository implements IMoviesRepository {
  @inject<IMoviesDbClient>(MOVIES_DB_CLIENT)
  private readonly moviesDbClient: IMoviesDbClient;

  public async create(movie: IMovie): Promise<void> {
    await this.moviesDbClient
      .getDb()
      .collection('movies')
      .insertOne(movie);
  }

  public async fetchAll(): Promise<Array<IMovie & { _id: string }>> {
    return this.moviesDbClient
      .getDb()
      .collection('movies')
      .find()
      .toArray();
  }
}
