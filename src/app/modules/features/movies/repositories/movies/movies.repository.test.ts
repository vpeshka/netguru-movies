import 'reflect-metadata';
import { MongoClient } from 'mongodb';
import { IMovie } from '../../interfaces';
import { container } from '@app/ioc/container';
import { MoviesRepository } from './movies.repository';
import { MOVIES_REPOSITORY } from './movies.repository.token';
import { IMoviesRepository } from './movies.repository.interface';
import { IMoviesDbClient, MOVIES_DB_CLIENT } from '@app/shared/db';

jest.mock('mongodb', () => {
  return {
    MongoClient: jest.fn().mockReturnValue({
      db: jest.fn().mockReturnValue({
        collection: jest.fn().mockReturnValue({
          insertOne: jest.fn(),
          find: jest.fn().mockReturnValue({
            toArray: jest.fn(),
          }),
        }),
      }),
    }),
  };
});

describe('Movies repository test cases.', () => {
  let repository: IMoviesRepository;
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const dbClientMock = new MongoClient();
  const collectionMock = dbClientMock.db().collection;
  const findMock = dbClientMock.db().collection('mock').find;
  const insertOneMock = dbClientMock.db().collection('mock').insertOne;
  const toArrayMock = dbClientMock.db().collection('mock').find().toArray;

  beforeAll(() => {
    container.unbindAll();

    container.bind(MOVIES_DB_CLIENT).toConstantValue({ getDb() { return dbClientMock.db(); } } as IMoviesDbClient);
    container.bind(MOVIES_REPOSITORY).to(MoviesRepository).inSingletonScope();

    repository = container.get<IMoviesRepository>(MOVIES_REPOSITORY);
  });

  beforeEach(() => {
    (collectionMock as jest.Mock).mockClear();
    (findMock as jest.Mock).mockClear();
    (insertOneMock as jest.Mock).mockClear();
    (toArrayMock as jest.Mock).mockClear();
  });

  afterAll(() => {
    container.unbindAll();
  });

  it('IMoviesRepository::create() method must call right call chain of mongo client.', async () => {
    const movie: IMovie = {
      title: 'Blade Runner',
      released: '25 Jun 1982',
      genre: 'Action, Sci-Fi, Thriller',
      director: 'Ridley Scott',
    };

    await repository.create(movie);

    expect(collectionMock).toBeCalledTimes(1);
    expect(collectionMock).toBeCalledWith('movies');

    expect(insertOneMock).toBeCalledTimes(1);
    expect(insertOneMock).toBeCalledWith(movie);
  });

  it('IMoviesRepository::fetchAll() method must call right call chain of mongo client.', async () => {
    await repository.fetchAll();

    expect(collectionMock).toBeCalledTimes(1);
    expect(collectionMock).toBeCalledWith('movies');

    expect(findMock).toBeCalledTimes(1);
    expect(findMock).toBeCalledWith();

    expect(toArrayMock).toBeCalledTimes(1);
    expect(toArrayMock).toBeCalledWith();
  });
});
