import { IMovie } from '../interfaces';
import { inject, injectable } from 'inversify';
import { IMoviesService } from './movies.service.interface';
import { IMoviesRepository, IOmdbRepository, MOVIES_REPOSITORY, OMDB_REPOSITORY } from '../repositories';

@injectable()
export class MoviesService implements IMoviesService {
  @inject<IMoviesRepository>(MOVIES_REPOSITORY)
  private readonly moviesRepository: IMoviesRepository;

  @inject<IOmdbRepository>(OMDB_REPOSITORY)
  private readonly omdbRepository: IOmdbRepository;

  public async create(title: string): Promise<void> {
    await this.moviesRepository.create(await this.omdbRepository.fetchMovieDetails(title));
  }

  public fetchAll(): Promise<Array<{ _id: string } & IMovie>> {
    return this.moviesRepository.fetchAll();
  }
}
