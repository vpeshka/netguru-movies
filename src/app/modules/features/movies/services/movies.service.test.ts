import 'reflect-metadata';
import { container } from '@app/ioc/container';
import { MoviesService } from './movies.service';
import { BadGateway } from '@vp_solutions/errors';
import { MOVIES_SERVICE } from './movies.service.token';
import { IMoviesService } from './movies.service.interface';
import { mustNotToBeCalledAssertion } from '@vp_solutions/express-skeleton-common-module';
import { IMoviesRepository, IOmdbRepository, MOVIES_REPOSITORY, OMDB_REPOSITORY } from '../repositories';

describe('Movies service test cases.', () => {
  let service: IMoviesService;

  const moviesRepoMock = {
    fetchAll: jest.fn(),
    create: jest.fn(),
  } as IMoviesRepository;

  const omdbRepoMock = {
    fetchMovieDetails: jest.fn(),
  } as IOmdbRepository;

  beforeAll(() => {
    container.unbindAll();

    container.bind(MOVIES_REPOSITORY).toConstantValue(moviesRepoMock);
    container.bind(OMDB_REPOSITORY).toConstantValue(omdbRepoMock);
    container.bind(MOVIES_SERVICE).to(MoviesService);

    service = container.get<IMoviesService>(MOVIES_SERVICE);
  });

  beforeEach(() => {
    (moviesRepoMock.fetchAll as jest.Mock).mockReset();
    (moviesRepoMock.create as jest.Mock).mockReset();
    (omdbRepoMock.fetchMovieDetails as jest.Mock).mockClear();
  });

  afterAll(() => {
    container.unbindAll();
  });

  it('IMoviesService::fetchAll() method must throw error from repo.', async () => {
    (moviesRepoMock.fetchAll as jest.Mock).mockRejectedValueOnce(new Error('repo error'));

    try {
      await service.fetchAll();
      mustNotToBeCalledAssertion();
    } catch (err: any) {
      expect(err).toBeInstanceOf(Error);
      expect(err.message).toEqual('repo error');
    }
  });

  it('IMoviesService::create() method must resolve movies list.', async () => {
    const moviesList = [{ id: 1 }];
    (moviesRepoMock.fetchAll as jest.Mock).mockResolvedValueOnce(moviesList);

    expect(await service.fetchAll()).toEqual(moviesList);
  });

  it('IMoviesService::create() method must throw error from repo.', async () => {
    (omdbRepoMock.fetchMovieDetails as jest.Mock).mockRejectedValueOnce(new BadGateway('repo error'));

    try {
      await service.create('movie');
      mustNotToBeCalledAssertion();
    } catch (err: any) {
      expect(err).toBeInstanceOf(BadGateway);
      expect(err.message).toEqual('repo error');
    }
  });

  it('IMoviesService::create() method must throw error from repo.', async () => {
    (moviesRepoMock.create as jest.Mock).mockRejectedValueOnce(new Error('repo error'));

    try {
      await service.create('movie');
      mustNotToBeCalledAssertion();
    } catch (err: any) {
      expect(err).toBeInstanceOf(Error);
      expect(err.message).toEqual('repo error');
    }
  });

  it('IMoviesService::create() method must be resolved as void value if no errors from repos.', async () => {
    (moviesRepoMock.create as jest.Mock).mockResolvedValueOnce([{ id: 1 }]);

    expect(await service.create('movie')).toBeUndefined();
  });
});
