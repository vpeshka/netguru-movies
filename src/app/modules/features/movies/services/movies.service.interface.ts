import { IMovie } from '../interfaces';

export interface IMoviesService {
  create(title: string): Promise<void>;
  fetchAll(): Promise<Array<{ _id: string } & IMovie>>;
}
