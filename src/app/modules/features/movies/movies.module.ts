import { interfaces, ContainerModule } from 'inversify';
import { IBaseRouter, IRestController } from '@vp_solutions/express-skeleton-common-module';

import { OmdbRepository } from './repositories/omdb/omdb.repository';
import { MoviesRepository } from './repositories/movies/movies.repository';
import { IMoviesRepository, IOmdbRepository, MOVIES_REPOSITORY, OMDB_REPOSITORY } from './repositories';

import { MOVIES_ROUTER } from './routers';
import { MoviesRouter } from './routers/movies.router';

import { MoviesService } from './services/movies.service';
import { IMoviesService, MOVIES_SERVICE } from './services';

import { MOVIES_CONTROLLER } from './controllers';
import { MoviesController } from './controllers/movies.controller';

export const getMoviesModule = (): interfaces.ContainerModule => {
  return new ContainerModule((bind: interfaces.Bind) => {
    bind<IBaseRouter>(MOVIES_ROUTER).to(MoviesRouter).inSingletonScope();
    bind<IRestController>(MOVIES_CONTROLLER).to(MoviesController).inSingletonScope();
    bind<IMoviesService>(MOVIES_SERVICE).to(MoviesService).inSingletonScope();
    bind<IMoviesRepository>(MOVIES_REPOSITORY).to(MoviesRepository).inSingletonScope();
    bind<IOmdbRepository>(OMDB_REPOSITORY).to(OmdbRepository).inSingletonScope();
  });
};
