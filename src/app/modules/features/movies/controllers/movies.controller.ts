import { constants } from 'http2';
import { inject, injectable } from 'inversify';
import { NextFunction, Request, Response } from 'express';
import { MOVIES_SERVICE, IMoviesService } from '../services';
import { IRestController, RestController } from '@vp_solutions/express-skeleton-common-module';

@injectable()
export class MoviesController extends RestController implements IRestController {
  @inject<IMoviesService>(MOVIES_SERVICE)
  private readonly moviesService: IMoviesService;

  public async create(req: Request, res: Response, _next: NextFunction): Promise<Response> {
    await this.moviesService.create(req.body.title);

    return res.status(constants.HTTP_STATUS_CREATED).send();
  }

  public async readAll(_req: Request, res: Response, _next: NextFunction): Promise<Response> {
    return res.status(constants.HTTP_STATUS_OK).json(await this.moviesService.fetchAll());
  }
}
