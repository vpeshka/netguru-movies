import { interfaces } from 'inversify';
import { IRestController } from '@vp_solutions/express-skeleton-common-module';

export const MOVIES_CONTROLLER: interfaces.ServiceIdentifier<IRestController> = Symbol.for('MOVIES_CONTROLLER');
