import 'reflect-metadata';
import { constants } from 'http2';
import { Request, Response } from 'express';
import { container } from '@app/ioc/container';
import { BadGateway } from '@vp_solutions/errors';
import { MoviesController } from './movies.controller';
import { IMoviesService, MOVIES_SERVICE } from '../services';
import { MOVIES_CONTROLLER } from './movies.controller.token';
import { IRestController, mustNotToBeCalledAssertion } from '@vp_solutions/express-skeleton-common-module';

describe('Movies controller test cases.', () => {
  let controller: IRestController;
  const jsonFuncMock = jest.fn();
  const sendFuncMock = jest.fn();
  const resMock = {
    status: jest.fn().mockReturnValue({ json: jsonFuncMock, send: sendFuncMock }),
  } as unknown as Response;
  const moviesServiceMock: IMoviesService = {
    fetchAll: jest.fn(),
    create: jest.fn(),
  };

  beforeAll(() => {
    container.unbindAll();

    container.bind(MOVIES_SERVICE).toConstantValue(moviesServiceMock);
    container.bind(MOVIES_CONTROLLER).to(MoviesController).inSingletonScope();

    controller = container.get<IRestController>(MOVIES_CONTROLLER);
  });

  beforeEach(() => {
    (moviesServiceMock.fetchAll as jest.Mock).mockReset();
    (moviesServiceMock.create as jest.Mock).mockReset();
    jsonFuncMock.mockClear();
    (resMock.status as jest.Mock).mockClear();
  });

  afterAll(() => {
    container.unbindAll();
  });

  it('IRestController::create() method must throw error if service throw error.', async () => {
    (moviesServiceMock.create as jest.Mock).mockRejectedValueOnce(new BadGateway('repo error'));

    try {
      await controller.create({ body: { title: 'title' } } as Request, resMock, jest.fn);
      mustNotToBeCalledAssertion();
    } catch (err: any) {
      expect(err).toBeInstanceOf(BadGateway);
      expect(err.message).toEqual('repo error');
    }
  });

  it(
    'IRestController::create() method must return empty response with 201 status code if movie created successfully.',
    async () => {
      const nextMiddleware = jest.fn();
      (moviesServiceMock.create as jest.Mock).mockResolvedValue(undefined);

      await controller.create({ body: { title: 'title' } } as Request, resMock, nextMiddleware);
      expect(resMock.status).toBeCalledTimes(1);
      expect(resMock.status).toBeCalledWith(constants.HTTP_STATUS_CREATED);
      expect(sendFuncMock).toBeCalledTimes(1);
      expect(sendFuncMock).toBeCalledWith();
    });

  it('IRestController::readAll() method must throw error if service throw error.', async () => {
    (moviesServiceMock.fetchAll as jest.Mock).mockRejectedValueOnce(new BadGateway('repo error'));

    try {
      await controller.readAll({} as Request, resMock, jest.fn);
      mustNotToBeCalledAssertion();
    } catch (err: any) {
      expect(err).toBeInstanceOf(BadGateway);
      expect(err.message).toEqual('repo error');
    }
  });

  it(
    'IRestController::create() method must return all movies with 200 status code if movies have fetched successfully.',
    async () => {
      const nextMiddleware = jest.fn();
      (moviesServiceMock.fetchAll as jest.Mock).mockResolvedValue([]);

      await controller.readAll({} as Request, resMock, nextMiddleware);
      expect(resMock.status).toBeCalledTimes(1);
      expect(resMock.status).toBeCalledWith(constants.HTTP_STATUS_OK);
      expect(jsonFuncMock).toBeCalledTimes(1);
      expect(jsonFuncMock).toBeCalledWith([]);
    });
});
