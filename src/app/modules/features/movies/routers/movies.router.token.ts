import { interfaces } from 'inversify';
import { IBaseRouter } from '@vp_solutions/express-skeleton-common-module';

export const MOVIES_ROUTER: interfaces.ServiceIdentifier<IBaseRouter> = Symbol.for('MOVIES_ROUTER');
