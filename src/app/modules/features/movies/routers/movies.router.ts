import {
  runAsync,
  BaseRouter,
  IBaseRouter,
  IRestController,
  validatePayloadBy,
} from '@vp_solutions/express-skeleton-common-module';
import { json } from 'body-parser';
import { RequestHandler } from 'express';
import { MOVIES_CONTROLLER } from '../controllers';
import { createMovieValidator } from '../validators';
import { IRateLimit, RATE_LIMIT } from '@app/shared/rl';
import { inject, injectable, postConstruct } from 'inversify';
import { AUTH_MIDDLEWARE, authorizeByRole } from '@app/features/auth';

@injectable()
export class MoviesRouter extends BaseRouter implements IBaseRouter {
  @inject<IRestController>(MOVIES_CONTROLLER)
  private readonly moviesController: IRestController;

  @inject(AUTH_MIDDLEWARE)
  private readonly authMiddleware: RequestHandler;

  @inject<IRateLimit>(RATE_LIMIT)
  private readonly rateLimit: IRateLimit;

  @postConstruct()
  protected defineRoutes(): void {
    this.router.post(
      '',
      runAsync(this.authMiddleware),
      runAsync(authorizeByRole()),
      runAsync(this.rateLimit.limit.bind(this.rateLimit)),
      json({ strict: true }),
      runAsync(validatePayloadBy(createMovieValidator)),
      runAsync(this.moviesController.create.bind(this.moviesController)),
    );

    this.router.get(
      '',
      runAsync(this.moviesController.readAll.bind(this.moviesController)),
    );
  }
}
