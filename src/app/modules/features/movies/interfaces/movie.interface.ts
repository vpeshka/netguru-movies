export interface IMovie {
  title: string;
  genre: string;
  released: string;
  director: string;
}
