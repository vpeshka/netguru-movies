import 'reflect-metadata';
import { createMovieValidator } from './create-movie.validator';
import { mustNotToBeCalledAssertion } from '@vp_solutions/express-skeleton-common-module';

describe('createMovieValidator test cases.', () => {
  it('Must be void if given value does not conform to the schema.', async () => {
    try {
      await createMovieValidator.validate({});
      mustNotToBeCalledAssertion();
    } catch (err: any) {
      expect(err).toBeInstanceOf(Error);
      expect(err.message).toEqual('Property \'title\' is required and cannot be empty.');
    }
  });

  it('Must be void if given value is not string.', async () => {
    try {
      await createMovieValidator.validate({ title: 1 });
      mustNotToBeCalledAssertion();
    } catch (err: any) {
      expect(err).toBeInstanceOf(Error);
      expect(err.message).toEqual('Field \'title\' must be \'string\', \'number\' given.');
    }
  });

  it('Must be void if given valid is too short.', async () => {
    try {
      await createMovieValidator.validate({ title: '1' });
      mustNotToBeCalledAssertion();
    } catch (err: any) {
      expect(err).toBeInstanceOf(Error);
      expect(err.message).toEqual('Field \'title\' must contain at least 3 symbols.');
    }
  });

  it('Must be void if given valid value.', async () => {
    expect(await createMovieValidator.validate({ title: '123' })).toBeUndefined();
  });
});
