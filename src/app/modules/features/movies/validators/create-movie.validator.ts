import { StringValidator, PayloadValidator } from '@vp_solutions/validators';

export const createMovieValidator = new PayloadValidator({
  title: {
    validator: new StringValidator({
      minLength: 3,
    }),
    isRequiredKey: true,
  },
});
