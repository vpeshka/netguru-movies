## Stage 1 - builder
FROM node:18-alpine as builder
RUN apk --no-cache add bash git
WORKDIR /usr/src/app
COPY package*.json /usr/src/app/
RUN npm install
COPY . /usr/src/app
RUN npm run dist:production

## Stage 2 - runner
FROM node:18-alpine as runner
RUN apk --no-cache add git
WORKDIR /usr/src/app
## Copy static files
COPY --from=builder /usr/src/app/dist /usr/src/app/dist/
COPY --from=builder /usr/src/app/package*.json /usr/src/app/
RUN npm install --production
CMD ["node", "./dist/service.js"]
